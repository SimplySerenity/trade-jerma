class CreateMinusTwos < ActiveRecord::Migration[6.0]
  def change
    create_table :minus_twos do |t|
      t.references :twitch_user, null: false, foreign_key: true
      t.string :message

      t.timestamps
    end
  end
end
