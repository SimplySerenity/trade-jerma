class CreateTwitchUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :twitch_users do |t|
      t.string :username, index: { unique: true }

      t.timestamps
    end
  end
end
