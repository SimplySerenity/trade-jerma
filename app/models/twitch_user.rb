class TwitchUser < ApplicationRecord
  has_many :plus_twos
  has_many :minus_twos
end
