class ChatbotController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :message_params
  before_action :set_user

  def update
    message = params[:message]
    if message[:text].include?('+2')
      @user.plus_twos.new(message: message[:text])
    else
      @user.minus_twos.new(message: message[:text])
    end
    @user.save
    render json: { username: @user.username, message: message[:text] }
  end

  private

  def message_params
    params.require(:message).permit(:username, :text)
  end

  def set_user
    @user = TwitchUser.find_by(username: params[:message][:username])
    unless @user.present?
      @user = TwitchUser.new(username: params[:message][:username])
      @user.save
    end
  end
end
