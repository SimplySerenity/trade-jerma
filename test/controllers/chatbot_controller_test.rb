require 'test_helper'

class ChatbotControllerTest < ActionDispatch::IntegrationTest
  test "should get update" do
    get chatbot_update_url
    assert_response :success
  end

end
